package com.example.owner.ma42_webview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class MainActivity extends AppCompatActivity {

     private WebView myview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //プログラム上でwebviewを使用可能にする
        myview = (WebView)findViewById(R.id.myweb);
        //自作クラスとwebViewの連動
        myview.setWebViewClient(new CustomBrowserClient());
        //webviewに対する設定
        //javascriptの有効化
        myview.getSettings().setJavaScriptEnabled(true);
        //拡大縮小コントロール追加
        myview.getSettings().setBuiltInZoomControls(true);
        //その他の設定
        //setSavePAssword : パスワード保存の有効・無効
        //setSaveFormData : フォームデータ保存の有効・無効

        //指定したURLサイトを読み込む
        myview.loadUrl("http://www.yahoo.co.jp");
    }
    //webviewに対して描画更新するための自作クラス
    //webviewClientクラスを継承したクラス
    private class CustomBrowserClient extends WebViewClient {
        //URLを更新したときの処理
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return false;
        }
    }

    //キーボタン(戻る)を押した時の処理

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        //戻るキーが押された時(KeyEvent.KEYCODE_BACK)
        if((keyCode == KeyEvent.KEYCODE_BACK) && (myview.canGoBack())){
            myview.canGoBack();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}
